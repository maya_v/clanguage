/*write a program to read a char and a num and print that char num of times*/
#include<stdio.h>
void program(char ch, int count){
 int i;
	for(i=0; i<count; i++){
		printf("%c ", ch);
	}
	printf("\n");
} 
int main(){
	program('a', 15);
	program('*', 10);
}
